import Dependencies._

lazy val root = (project in file("."))
  .settings(
    name := "clr-models",
    description := "Clairsienne models",
    homepage := Some(url("https://gitlab.com/clairsienne/clr-models")),
    ThisBuild / scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/clairsienne/clr-parsers/clr-parsers.git"),
        "scm:git@gitlab.com/clairsienne/clr-parsers/clr-parsers.git"
      )
    ),
    libraryDependencies ++= Seq(
      playJson,
      akkaHttpSprayJson % Provided,
      akkaHttpPlayJson % Provided,
      rdf4jRuntime
    )
  )
