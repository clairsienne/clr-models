import sbt._

object Versions {
  lazy val akkaHttpVersion = "10.1.11"
  lazy val rdf4jVersion = "2.5.0"
}

object Dependencies {
  lazy val playJson = "com.typesafe.play" %% "play-json" % "2.7.0"
  lazy val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % Versions.akkaHttpVersion
  lazy val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % "1.30.0"
  lazy val rdf4jRuntime = "org.eclipse.rdf4j" % "rdf4j-runtime" % Versions.rdf4jVersion
}
